#include "Shader.h"

CShader::CShader()
{
}

CShader::~CShader()
{
	if (m_ppd3dPipelineStates)
	{
		for (int i = 0; i < m_nPipelineStates; i++) if (m_ppd3dPipelineStates[i])
			m_ppd3dPipelineStates[i]->Release();
		delete[] m_ppd3dPipelineStates;
	}
}

// 래스터라이저 상태를 설정하기 위한 구조체를 반환한다.
D3D12_RASTERIZER_DESC CShader::CreateRasterizerState()
{
	D3D12_RASTERIZER_DESC d3dRasterizerDesc;
	::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0;
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
	return(d3dRasterizerDesc);
}

// 깊이-스텐실 검사를 위한 상태를 설정하기 위한 구조체를 반환한다.
D3D12_DEPTH_STENCIL_DESC CShader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
	::ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	d3dDepthStencilDesc.DepthEnable = TRUE;
	d3dDepthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	d3dDepthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	d3dDepthStencilDesc.StencilEnable = FALSE;
	d3dDepthStencilDesc.StencilReadMask = 0x00;
	d3dDepthStencilDesc.StencilWriteMask = 0x00;
	d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	d3dDepthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	return(d3dDepthStencilDesc);
}

// 블렌딩 상태를 설정하기 위한 구조체를 반환한다.
D3D12_BLEND_DESC CShader::CreateBlendState()
{
	D3D12_BLEND_DESC d3dBlendDesc;
	::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
	d3dBlendDesc.AlphaToCoverageEnable = FALSE;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(d3dBlendDesc);
}

// 입력 조립기에게 정점 버퍼의 구조를 알려주기 위한 구조체를 반환한다.
D3D12_INPUT_LAYOUT_DESC CShader::CreateInputLayout()
{
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = NULL;
	d3dInputLayoutDesc.NumElements = 0;
	return(d3dInputLayoutDesc);
}

// 정점 셰이더 바이트 코드를 생성(컴파일)한다.
D3D12_SHADER_BYTECODE CShader::CreateVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	D3D12_SHADER_BYTECODE d3dShaderByteCode;
	d3dShaderByteCode.BytecodeLength = 0;
	d3dShaderByteCode.pShaderBytecode = NULL;
	return(d3dShaderByteCode);
}

// 픽셀 셰이더 바이트 코드를 생성(컴파일)한다.
D3D12_SHADER_BYTECODE CShader::CreatePixelShader(ID3DBlob** ppd3dShaderBlob)
{
	D3D12_SHADER_BYTECODE d3dShaderByteCode;
	d3dShaderByteCode.BytecodeLength = 0;
	d3dShaderByteCode.pShaderBytecode = NULL;
	return(d3dShaderByteCode);
}

// 셰이더 소스 코드를 컴파일하여 바이트 코드 구조체를 반환한다.
D3D12_SHADER_BYTECODE CShader::CompileShaderFromFile(const WCHAR* pszFileName, LPCSTR
	pszShaderName, LPCSTR pszShaderProfile, ID3DBlob** ppd3dShaderBlob)
{
	UINT nCompileFlags = 0;
#if defined(_DEBUG)
	nCompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	::D3DCompileFromFile(pszFileName, NULL, NULL, pszShaderName, pszShaderProfile,
		nCompileFlags, 0, ppd3dShaderBlob, NULL);
	D3D12_SHADER_BYTECODE d3dShaderByteCode;
	d3dShaderByteCode.BytecodeLength = (*ppd3dShaderBlob)->GetBufferSize();
	d3dShaderByteCode.pShaderBytecode = (*ppd3dShaderBlob)->GetBufferPointer();
	return(d3dShaderByteCode);
}

//그래픽스 파이프라인 상태 객체를 생성한다.
void CShader::CreateShader(ID3D12Device* pd3dDevice, ID3D12RootSignature
	* pd3dRootSignature)
{
	ID3DBlob* pd3dVertexShaderBlob = NULL, * pd3dPixelShaderBlob = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC d3dPipelineStateDesc;
	::ZeroMemory(&d3dPipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	d3dPipelineStateDesc.pRootSignature = pd3dRootSignature;
	d3dPipelineStateDesc.VS = CreateVertexShader(&pd3dVertexShaderBlob);
	d3dPipelineStateDesc.PS = CreatePixelShader(&pd3dPixelShaderBlob);
	d3dPipelineStateDesc.RasterizerState = CreateRasterizerState();
	d3dPipelineStateDesc.BlendState = CreateBlendState();
	d3dPipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	d3dPipelineStateDesc.InputLayout = CreateInputLayout();
	d3dPipelineStateDesc.SampleMask = UINT_MAX;
	d3dPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	d3dPipelineStateDesc.NumRenderTargets = 1;
	d3dPipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	d3dPipelineStateDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dPipelineStateDesc.SampleDesc.Count = 1;
	d3dPipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	pd3dDevice->CreateGraphicsPipelineState(&d3dPipelineStateDesc,
		__uuidof(ID3D12PipelineState), (void**)&m_ppd3dPipelineStates[0]);

	if (pd3dVertexShaderBlob) pd3dVertexShaderBlob->Release();
	if (pd3dPixelShaderBlob) pd3dPixelShaderBlob->Release();
	if (d3dPipelineStateDesc.InputLayout.pInputElementDescs) delete[]
		d3dPipelineStateDesc.InputLayout.pInputElementDescs;
}

void CShader::OnPrepareRender(ID3D12GraphicsCommandList* pd3dCommandList)
{
	// 파이프라인에 그래픽스 상태 객체를 설정한다.
	pd3dCommandList->SetPipelineState(m_ppd3dPipelineStates[0]);
}

void CShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera)
{
	OnPrepareRender(pd3dCommandList);
}

void CShader::CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList
	* pd3dCommandList)
{

}

void CShader::UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList)
{

}

void CShader::UpdateShaderVariable(ID3D12GraphicsCommandList* pd3dCommandList, XMFLOAT4X4* pxmf4x4World)
{
	XMFLOAT4X4 xmf4x4World;
	XMStoreFloat4x4(&xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(pxmf4x4World)));
	pd3dCommandList->SetGraphicsRoot32BitConstants(0, 16, &xmf4x4World, 0);
}

void CShader::ReleaseShaderVariables()
{
}

CPlayerShader::CPlayerShader()
{
}

CPlayerShader::~CPlayerShader()
{
}

D3D12_INPUT_LAYOUT_DESC CPlayerShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12,
	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;
	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CPlayerShader::CreateVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CPlayerShader::CreatePixelShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", ppd3dShaderBlob));
}

void CPlayerShader::CreateShader(ID3D12Device* pd3dDevice, ID3D12RootSignature
	* pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState * [m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

CObjectsShader::CObjectsShader()
{
}

CObjectsShader::~CObjectsShader()
{
}

void CObjectsShader::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList)
{
	XMFLOAT3 rotAxislocal;
	XMFLOAT3 movDirlocal;

	// 여러 개의 메쉬
	CCubeMeshDiffused* pCubeMesh0 = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList,
		10.0f, 10.0f, 10.0f, XMFLOAT4(1.f, 0.f, 0.f, 1.f));
	CCubeMeshDiffused* pCubeMesh1 = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList,
		14.0f, 14.0f, 14.0f, XMFLOAT4(1.f, 1.f, 0.f, 1.f));
	CCubeMeshDiffused* pCubeMesh2 = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList,
		8.0f, 8.0f, 8.0f, XMFLOAT4(1.f, 0.f, 1.f, 1.f));
	CCubeMeshDiffused* pCubeMesh3 = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList,
		12.f, 12.f, 12.f, XMFLOAT4(0.f, 0.f, 1.f, 1.f));
	CCubeMeshDiffused* pCubeMesh4 = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList,
		16.f, 16.f, 16.f, XMFLOAT4(0.f, 1.f, 0.f, 1.f));
	CCubeMeshDiffused* pCubeMesh5 = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList,
		2.f, 2.f, 2.f, XMFLOAT4(0.f, 1.f, 1.f, 1.f));

	m_nObjects = 8;
	m_ppObjects = new CGameObject * [m_nObjects];

	CRotatingObject* pRotatingObject0 = NULL;
	pRotatingObject0 = new CRotatingObject;

	rotAxislocal = XMFLOAT3(1.0f, 1.0f, 0.0f);
	movDirlocal = XMFLOAT3(1.0f, 0.0f, 0.0f);
	pRotatingObject0->SetMesh(pCubeMesh0);
	pRotatingObject0->SetPosition(-33.5f, 0.0f, 54.0f);
	pRotatingObject0->SetRotationAxis(rotAxislocal);
	pRotatingObject0->SetRotationSpeed(90.0f);
	pRotatingObject0->SetMovingDirection(movDirlocal);
	pRotatingObject0->SetMovingSpeed(5.5f);
	m_ppObjects[0] = pRotatingObject0;

	CRotatingObject* pRotatingObject1 = new CRotatingObject;
	rotAxislocal = XMFLOAT3(0.0f, 1.0f, 1.0f);
	movDirlocal = XMFLOAT3(-1.0f, 0.0f, 0.0f);
	pRotatingObject1->SetMesh(pCubeMesh1);
	pRotatingObject1->SetPosition(+23.5f, 10.0f, 114.0f);
	pRotatingObject1->SetRotationAxis(rotAxislocal);
	pRotatingObject1->SetRotationSpeed(180.0f);
	pRotatingObject1->SetMovingDirection(movDirlocal);
	pRotatingObject1->SetMovingSpeed(3.5f);
	m_ppObjects[1] = pRotatingObject1;

	CRotatingObject* pRotatingObject2 = new CRotatingObject;
	rotAxislocal = XMFLOAT3(1.0f, 0.0f, 1.0f);
	movDirlocal = XMFLOAT3(1.0f, -1.0f, 0.0f);
	pRotatingObject2->SetMesh(pCubeMesh2);
	pRotatingObject2->SetPosition(0.0f, +5.0f, 30.0f);
	pRotatingObject2->SetRotationAxis(rotAxislocal);
	pRotatingObject2->SetRotationSpeed(30.15f);
	pRotatingObject2->SetMovingDirection(movDirlocal);
	pRotatingObject2->SetMovingSpeed(2.0f);
	m_ppObjects[2] = pRotatingObject2;

	CRotatingObject* pRotatingObject3 = new CRotatingObject;
	rotAxislocal = XMFLOAT3(0.0f, 0.0f, 1.0f);
	movDirlocal = XMFLOAT3(0.0f, 0.0f, 1.0f);
	pRotatingObject3->SetMesh(pCubeMesh3);
	pRotatingObject3->SetPosition(-10.0f, -20.0f, 35.0f);
	pRotatingObject3->SetRotationAxis(rotAxislocal);
	pRotatingObject3->SetRotationSpeed(40.6f);
	pRotatingObject3->SetMovingDirection(movDirlocal);
	pRotatingObject3->SetMovingSpeed(4.5f);
	m_ppObjects[3] = pRotatingObject3;

	CRotatingObject* pRotatingObject4 = new CRotatingObject;
	rotAxislocal = XMFLOAT3(0.0f, 1.0f, 1.0f);
	movDirlocal = XMFLOAT3(0.0f, 1.0f, 1.0f);
	pRotatingObject4->SetMesh(pCubeMesh4);
	pRotatingObject4->SetPosition(30.0f, -30.0f, 58.0f);
	pRotatingObject4->SetRotationAxis(rotAxislocal);
	pRotatingObject4->SetRotationSpeed(50.06f);
	pRotatingObject4->SetMovingDirection(movDirlocal);
	pRotatingObject4->SetMovingSpeed(4.5f);
	m_ppObjects[4] = pRotatingObject4;

	CBullet* pBullet0 = new CBullet;
	pBullet0->SetMesh(pCubeMesh5);
	pBullet0->SetPosition(0.f, 0.f, 0.f);
	pBullet0->SetMovingSpeed(300.f);
	m_ppObjects[5] = pBullet0;

	CBullet* pBullet1 = new CBullet;
	pBullet1->SetMesh(pCubeMesh5);
	pBullet1->SetPosition(0.f, 0.f, 0.f);
	pBullet1->SetMovingSpeed(300.f);
	m_ppObjects[6] = pBullet1;

	CBullet* pBullet2 = new CBullet;
	pBullet2->SetMesh(pCubeMesh5);
	pBullet2->SetPosition(0.f, 0.f, 0.f);
	pBullet2->SetMovingSpeed(300.f);
	m_ppObjects[7] = pBullet2;

	/*
	CBullet* pBullet3 = new CBullet;
	pBullet3->SetMesh(pCubeMesh5);
	pBullet3->SetPosition(0.f, 0.f, 0.f);
	pBullet3->SetMovingSpeed(300.f);
	m_ppObjects[8] = pBullet3;

	CBullet* pBullet4 = new CBullet;
	pBullet4->SetMesh(pCubeMesh5);
	pBullet4->SetPosition(0.f, 0.f, 0.f);
	pBullet4->SetMovingSpeed(300.f);
	m_ppObjects[9] = pBullet4;

	CBullet* pBullet5 = new CBullet;
	pBullet5->SetMesh(pCubeMesh5);
	pBullet5->SetPosition(0.f, 0.f, 0.f);
	pBullet5->SetMovingSpeed(300.f);
	m_ppObjects[10] = pBullet5;

	CBullet* pBullet6 = new CBullet;
	pBullet6->SetMesh(pCubeMesh5);
	pBullet6->SetPosition(0.f, 0.f, 0.f);
	pBullet6->SetMovingSpeed(300.f);
	m_ppObjects[11] = pBullet6;

	CBullet* pBullet7 = new CBullet;
	pBullet7->SetMesh(pCubeMesh5);
	pBullet7->SetPosition(0.f, 0.f, 0.f);
	pBullet7->SetMovingSpeed(300.f);
	m_ppObjects[12] = pBullet7;

	CBullet* pBullet8 = new CBullet;
	pBullet8->SetMesh(pCubeMesh5);
	pBullet8->SetPosition(0.f, 0.f, 0.f);
	pBullet8->SetMovingSpeed(300.f);
	m_ppObjects[13] = pBullet8;

	CBullet* pBullet9 = new CBullet;
	pBullet9->SetMesh(pCubeMesh5);
	pBullet9->SetPosition(0.f, 0.f, 0.f);
	pBullet9->SetMovingSpeed(300.f);
	m_ppObjects[14] = pBullet9;
	*/

	CreateShaderVariables(pd3dDevice, pd3dCommandList);
}

void CObjectsShader::AnimateObjects(float fTimeElapsed)
{
	for (int j = 0; j < m_nObjects; j++)
	{
		m_ppObjects[j]->Animate(fTimeElapsed);
	}
}

void CObjectsShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
		{
			if (m_ppObjects[j]) delete m_ppObjects[j];
		}
		delete[] m_ppObjects;
	}
}

D3D12_INPUT_LAYOUT_DESC CObjectsShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12,
	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;
	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CObjectsShader::CreateVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CObjectsShader::CreatePixelShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", ppd3dShaderBlob));
}

void CObjectsShader::CreateShader(ID3D12Device* pd3dDevice, ID3D12RootSignature* pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState * [m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CObjectsShader::ReleaseUploadBuffers()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++) m_ppObjects[j]->ReleaseUploadBuffers();
	}
}

void CObjectsShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera)
{
	CShader::Render(pd3dCommandList, pCamera);
	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j])
		{
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);
		}
	}
}
