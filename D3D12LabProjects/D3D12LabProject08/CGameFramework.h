#pragma once
#include "Timer.h"
#include "Scene.h"
#include "Camera.h"

/* 
	게임 프로그램의 기본적 요소를 추가하기 위해 하나의 클래스(Class)를 만들도록 하자.
클래스의 이름은 “CGameFramework”이다. 이 클래스는 게임 프로그램이 가져야 될 기본적인 내용(형태)
을 표현한다. 

이 ”CGameFramework” 클래스 객체는 게임 프로그램의 뼈대를 나타낸다(게임 프레임워크). 이 클래스는
Direct3D 디바이스를 생성하고 관리하며 화면 출력을 위한 여러 가지 처리(게임 객체의 생성과 관리, 사
용자 입력, 애니메이션 등)를 담당한다.

	- 따라하기 03
*/

class CGameFramework
{
private:
	HINSTANCE m_hInstance;
	HWND m_hWnd;

	int m_nWndClientWidth;
	int m_nWndClientHeight;

	CGameTimer m_GameTimer;				// 게임 프레임워크에서 사용할 타이머
	_TCHAR m_pszFrameRate[50];			// 프레임 레이트를 주 윈도우의 캡션에 출력하기 위한 문자열

	CScene* m_pScene;

	IDXGIFactory4* m_pdxgiFactory;		//	DXGI 팩토리 인터페이스 포인터
	IDXGISwapChain3* m_pdxgiSwapChain;	//	스왑 체인 인터페이스 포인터, 디스플레이 제어를 위해 필요
	ID3D12Device* m_pd3dDevice;			//	다이렉트3D 12 디바이스 인터페이스 포인터 , 리소스 생성을 위해 필요

	bool m_bMSAA4xEnable = false;
	UINT m_nMSAA4xQualityLevels = 0;	//	MSAA 다중 샘플링 활성화, 다중 샘플링 레벨 설정

	static const UINT m_nSwapChainBuffers = 2;	//	스왑 체인 후면 버퍼 개수
	UINT m_nSwapChainBufferIndex;				//	현재 스왑 체인 후면 버퍼 인덱스

	ID3D12Resource* m_ppd3dRenderTargetBuffers[m_nSwapChainBuffers];	//	렌더 타겟 버퍼 인터페이스 포인터
	ID3D12DescriptorHeap* m_pd3dRtvDescriptorHeap;						//	서술자 힙 인터페이스 포인터
	UINT m_nRtvDescriptorIncrementSize;									//	렌더 타겟 서술자 원소의 크기

	ID3D12Resource* m_pd3dDepthStencilBuffer;		//	뎁스-스텐실 버퍼 인터페이스 포인터
	ID3D12DescriptorHeap* m_pd3dDsvDescriptorHeap;	//	서술자 힙 인터페이스 포인터
	UINT m_nDsvDescriptorIncrementSize;				//	뎁스-스텐실 서술자 원소의 크기

	ID3D12CommandQueue* m_pd3dCommandQueue;			//	명령 큐 인터페이스 포인터
	ID3D12CommandAllocator* m_pd3dCommandAllocator;	//	명령 할당자 인터페이스 포인터
	ID3D12GraphicsCommandList* m_pd3dCommandList;	//	명령 리스트 인터페이스 포인터

	ID3D12PipelineState* m_pd3dPipelineState;		//	그래픽스 파이프라인 상태 객체에 대한 인터페이스 포인터

	ID3D12Fence* m_pd3dFence;						//	펜스 인터페이스 포인터
	UINT64 m_nFenceValues[m_nSwapChainBuffers];		//	펜스 값
	HANDLE m_hFenceEvent;							//	이벤트 핸들

public:
	CCamera* m_pCamera = NULL;

	CGameFramework();
	~CGameFramework();

	//	주 윈도우가 생성되면 호출되어 프레임워크 객체 초기화
	bool OnCreate(HINSTANCE hInstance, HWND hWnd);		

	void OnDestroy();

	//	스왑 체인 생성
	void CreateSwapChain();
	//	전체화면을 위한 스왑체인 상태 변경
	void ChangeSwapChainState();


	//	Direct3D 디바이스 생성
	void CreateDirect3DDevice();

	//	명령 큐, 명령 할당자, 명령 리스트 생성
	void CreateCommandQueueAndList();					

	//	렌더 타겟, 뎁스-스텐실 서술자 힙 생성
	void CreateRtvAndDsvDescriptorHeaps();

	//	렌더 타겟 뷰들을 생성
	void CreateRenderTargetViews();

	//	뎁스-스텐실 뷰를 생성
	void CreateDepthStencilView();						


	//	렌더링 할 메쉬 및 게임 객체 생성
	void BuildObjects();

	//	메쉬와 게임 객체 소멸
	void ReleaseObjects();								


	/* 게임 프레임워크의 핵심 */
	//	사용자 입력 처리 함수
	void ProcessInput();

	//	객체 애니메이션 처리 함수
	void AnimateObjects();

	//	렌더링과 프레임 처리 함수
	void FrameAdvance();
	void MoveToNextFrame();

	//	CPU, GPU 동기화 함수
	void WaitForGPUComplete();							


	/* 윈도우 메세지 처리 */
	//	마우스 메세지 처리 함수
	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	//	키보드 메세지 처리 함수
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	
	//	윈도우 메세지 처리 함수
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
};

