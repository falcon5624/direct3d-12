﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")

#pragma comment(lib, "dxguid.lib")

#pragma comment(lib, "winmm.lib")

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <Windows.h>
#include <timeapi.h>
// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <mmsystem.h>

//	Direct3D 라이브러리 헤더입니다.
#include <string>
#include <wrl.h>
#include <shellapi.h>

#include <d3d12.h>
#include <dxgi1_4.h>

#include <d3dcompiler.h>

#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>

#include <dxgidebug.h>

using namespace DirectX;
using namespace DirectX::PackedVector;

using Microsoft::WRL::ComPtr;

/*
	“stdafx.h" 파일에 헤더 파일들을 추가하는 이유는 Visual Studio에서는 PCH(Precompiled Header)를
통하여 자주 변경되지 않는 헤더 파일에 대한 컴파일을 매번 하지 않아도 되도록 하는 기능을 제공하기
때문이다.	-	따라하기 03
*/

/*	이곳에 WinMain에 들어갈 윈도우 클래스를 규정하는 매크로 등을 규정합니다.
*/

/* 전체화면 상수 정의 시, 시작하면 전체화면으로 */
// #define _WITH_SWAPCHAIN_FULLSCREEN_STATE

constexpr int FRAMEBUFF_WIDTH = 800;
constexpr int FRAMEBUFF_HEIGHT = 600;

constexpr int CLIENT_WIDTH = FRAMEBUFF_WIDTH;
constexpr int CLIENT_HEIGHT = FRAMEBUFF_HEIGHT;

//	모니터 크기를 계산합니다.
const int MAX_MONITOR_WIDTH{ GetSystemMetrics(SM_CXSCREEN) };
const int MAX_MONITOR_HEIGHT{ GetSystemMetrics(SM_CYSCREEN) };

extern ID3D12Resource* CreateBufferResource(ID3D12Device* pd3dDevice, 
	ID3D12GraphicsCommandList* pd3dCommandList, void* pData, UINT nBytes,
	D3D12_HEAP_TYPE d3dHeapType = D3D12_HEAP_TYPE_UPLOAD,
	D3D12_RESOURCE_STATES d3dResourceStates = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER,
	ID3D12Resource** ppd3dUploadBuffer = NULL);