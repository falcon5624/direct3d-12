#pragma once
#include "GameObject.h"
#include "Camera.h"

class CPlayer : public CGameObject
{
public:
	CCamera* m_pCamera = NULL;

	XMFLOAT3 m_xmf3Position;
	XMFLOAT3 m_xmf3Right;
	XMFLOAT3 m_xmf3Up;
	XMFLOAT3 m_xmf3Look;

	XMFLOAT3 m_xmf3CameraOffset;
	XMFLOAT3 m_xmf3Velocity;
	float m_fFriction = 125.f;

	float m_fPitch = 0.0f;
	float m_fYaw = 0.0f;
	float m_fRoll = 0.0f;

public:
	CPlayer();
	virtual ~CPlayer() { if (m_pCamera) delete m_pCamera; }

	/* Set */
	void SetPosition(float x, float y, float z);
	void SetRotation(float pitch, float yaw, float roll);
	void SetCamera(CCamera* pCamera) { m_pCamera = pCamera; }
	void SetCameraOffset(XMFLOAT3& xmf3CameraOffset);

	void LookAt(XMFLOAT3& xmf3LookAt, XMFLOAT3& xmf3Up);

	/* Get */
	// Getter for Private Properties 
	CCamera* GetCamera() { return m_pCamera; }

	void Move(DWORD dwDirection, float fDistance);
	void Move(XMFLOAT3& xmf3Shift, bool bUpdateVelocity);
	void Move(float x, float y, float z);
	void Rotate(float fPitch, float fYaw, float fRoll);

	/* Logic */
	void Update(float fTimeElapsed = 0.016f);

	virtual void OnUpdateTransform();
	virtual void Animate(float fElapsedTime);
};

class CAirplanePlayer : public CPlayer
{
public:
	CAirplanePlayer();
	virtual ~CAirplanePlayer();
	virtual void OnUpdateTransform();
};
