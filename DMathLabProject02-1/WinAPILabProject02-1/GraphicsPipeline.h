#pragma once
#include "GameObject.h"
#include "Camera.h"

class CGraphicsPipeline
{
private:
	//	현재 렌더링할 객체의 월드변환행렬
	static XMFLOAT4X4* m_pxmf4x4WorldMatrix;
	//	현재 카메라 객체의 카메라변환행렬, 원근투영변환행렬의 행렬곱
	static XMFLOAT4X4* m_pxmf4x4ViewProjectionMatrix;
	static CViewport* m_pViewport;

public:
	static void SetWorldTransform(XMFLOAT4X4* pxmf4x4WorldMatrix) {
		m_pxmf4x4WorldMatrix = pxmf4x4WorldMatrix;
	}
	static void SetViewProjectTransform(XMFLOAT4X4* pxmf4x4ViewProjectionMatrix)
	{
		m_pxmf4x4ViewProjectionMatrix = pxmf4x4ViewProjectionMatrix;
	}
	static void SetViewport(CViewport* pViewport) {
		m_pViewport = pViewport;
	}
	static XMFLOAT3 ScreenTransform(XMFLOAT3& xmf3Project);
	static XMFLOAT3 Project(XMFLOAT3& xmf3Model);
};

