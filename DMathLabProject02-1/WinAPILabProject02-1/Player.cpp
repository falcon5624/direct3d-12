#include "Player.h"

CPlayer::CPlayer()
{
	m_pCamera = new CCamera();

	m_xmf3Position = XMFLOAT3(0.f, 0.f, 0.f);
	m_xmf3Look = XMFLOAT3(0.f, 0.f, 1.0);
	m_xmf3Up = XMFLOAT3(0.f, 1.f, 0.f);
	m_xmf3Right = XMFLOAT3(1.f, 0.f, 0.f);
	m_xmf3CameraOffset = XMFLOAT3(0.f, 0.f, 0.f);
	m_xmf3Velocity = XMFLOAT3(0.f, 0.f, 0.f);
}

void CPlayer::SetPosition(float x, float y, float z)
{
	/* 플레이어와 카메라의 위치 설정 */
	CGameObject::SetPosition(x, y, z);
	m_xmf3Position = XMFLOAT3(x, y, z);
}

void CPlayer::SetRotation(float pitch, float yaw, float roll)
{
	/* 플레이어와 카메라의 회전각도 설정 */
	//if (m_pCamera) m_pCamera->SetRotation(pitch, yaw, roll);
}

void CPlayer::SetCameraOffset(XMFLOAT3& xmf3CameraOffset)
{
	m_xmf3CameraOffset = xmf3CameraOffset;
	XMFLOAT3 xmf3CameraPosition = Vector3::Add(m_xmf3Position, m_xmf3CameraOffset);
	m_pCamera->SetLookAtWithPosition(xmf3CameraPosition, m_xmf3Position, m_xmf3Up);
	m_pCamera->GenerateViewMatrix();
}

void CPlayer::LookAt(XMFLOAT3& xmf3LookAt, XMFLOAT3& xmf3Up)
{
	XMFLOAT4X4 xmf4x4ViewMatrix = Matrix4x4::LookAtLH(m_xmf3Position, xmf3LookAt, xmf3Up);

	m_xmf3Look = Vector3::Normalize(XMFLOAT3(xmf4x4ViewMatrix._11, xmf4x4ViewMatrix._21, xmf4x4ViewMatrix._31));
	m_xmf3Up = Vector3::Normalize(XMFLOAT3(xmf4x4ViewMatrix._12, xmf4x4ViewMatrix._22, xmf4x4ViewMatrix._32));
	m_xmf3Right = Vector3::Normalize(XMFLOAT3(xmf4x4ViewMatrix._13, xmf4x4ViewMatrix._23, xmf4x4ViewMatrix._33));
}

void CPlayer::Move(DWORD dwDirection, float fDistance)
{
	/* 교수님 설명 : 플레이어의 위치를 변경하는 함수이다. 플레이어의 위치는 기본적으로 사용자가 플레이어를 이동하기
위한 키보드를 누를 때 변경된다. 플레이어의 이동 방향(dwDirection)에 따라 플레이어를 fDistance 만
큼 이동한다. */
	if (dwDirection)
	{
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

		//	화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_FORWARD) xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, fDistance));
		if (dwDirection & DIR_BACKWARD) xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Look, -fDistance));

		//	화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_RIGHT) xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, fDistance));
		if (dwDirection & DIR_LEFT) xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Right, -fDistance));

		//	‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.
		if (dwDirection & DIR_UP) xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Up, fDistance));
		if (dwDirection & DIR_DOWN) xmf3Shift = Vector3::Add(xmf3Shift, Vector3::ScalarProduct(m_xmf3Up, -fDistance));

		//	플레이어를 현재 위치 벡터에서 xmf3Shift 벡터만큼 이동한다.
		this->Move(xmf3Shift, true);
	}
}

void CPlayer::Move(XMFLOAT3& xmf3Shift, bool bUpdateVelocity)
{
	/* bUpdateVelocity가 참이면 플레이어를 이동하지 않고 속도 벡터를 변경한다. */
	if (bUpdateVelocity) {
		m_xmf3Velocity = Vector3::Add(m_xmf3Velocity, xmf3Shift);
	} else {
		m_xmf3Position = Vector3::Add(xmf3Shift, m_xmf3Position);
		if (m_pCamera) m_pCamera->Move(xmf3Shift);
	}
}

void CPlayer::Move(float x, float y, float z)
{
	XMFLOAT3 position = XMFLOAT3(x, y, z);
	Move(position, false);
}

void CPlayer::Rotate(float fPitch, float fYaw, float fRoll)
{
	m_pCamera->Rotate(fPitch, fYaw, fRoll);

	if (!isZero(fPitch)) {
		XMFLOAT4X4 xmf4x4RotationMatrix =
			Matrix4x4::RotationAxis(m_xmf3Right, fPitch);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4RotationMatrix);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4RotationMatrix);
	}
	if (!isZero(fYaw)) {
		XMFLOAT4X4 xmf4x4RotationMatrix =
			Matrix4x4::RotationAxis(m_xmf3Up, fYaw);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4RotationMatrix);
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4RotationMatrix);
	}
	if (!isZero(fRoll)) {
		XMFLOAT4X4 xmf4x4RotationMatrix =
			Matrix4x4::RotationAxis(m_xmf3Look, fRoll);
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4RotationMatrix);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4RotationMatrix);
	}

	/* 교수님 설명 : 회전으로 인해 플레이어의 로컬 x-축, y-축, z-축이 서로 직교하지 않을 수 있으므로 
	z-축(Look 벡터)을 기준으로 하여 서로 직교하고 단위벡터가 되도록 한다. */
	m_xmf3Look = Vector3::Normalize(m_xmf3Look);
	m_xmf3Right = Vector3::Normalize(Vector3::CrossProduct(m_xmf3Up, m_xmf3Look));
	m_xmf3Up = Vector3::Normalize(Vector3::CrossProduct(m_xmf3Look, m_xmf3Right));
}

void CPlayer::Update(float fTimeElapsed)
{
	/* 교수님 설명 : 이 함수는 매 프레임마다 프레임워크에서 호출된다. 플레이어의 속도 벡터에 중력과 마찰력 등을 적용
	하여 플레이어를 이동한다. */

	Move(m_xmf3Velocity, false);

	m_pCamera->Update(this, m_xmf3Position, fTimeElapsed);
	m_pCamera->GenerateViewMatrix();

	/*	플레이어의 속도 벡터가 마찰력 때문에 감속이 되어야 한다면 감속 벡터를 생성한다. 
	속도 벡터의 반대 방향 벡터를 구하고 단위 벡터로 만든다. 마찰 계수를 시간에 비례하도록 하여 마찰력을 구한다. 
	단위 벡터에 마찰력을 곱하여 감속 벡터를 구한다. 속도 벡터에 감속 벡터를 더하여 속도 벡터를 줄인다.
	마찰력이 속력보다 크면 속력은 0이 될 것이다.*/

	XMFLOAT3 xmf3Deceleration = Vector3::Normalize(Vector3::ScalarProduct(m_xmf3Velocity, -1.f));
	float fLength = Vector3::GetLength(xmf3Deceleration);
	float fDeceleration = m_fFriction * fTimeElapsed;
	if (fDeceleration > fLength) fDeceleration = fLength;

	m_xmf3Velocity = Vector3::Add(m_xmf3Velocity, Vector3::ScalarProduct(xmf3Deceleration, fDeceleration));

}

void CPlayer::OnUpdateTransform()
{
	/* 플레이어의 위치, 방향 벡터로부터 월드변환행렬을 구한다. */

	m_xmf4x4WorldMatrix._11 = m_xmf3Right.x;
	m_xmf4x4WorldMatrix._12 = m_xmf3Right.y;
	m_xmf4x4WorldMatrix._13 = m_xmf3Right.z;
	m_xmf4x4WorldMatrix._21 = m_xmf3Up.x;
	m_xmf4x4WorldMatrix._22 = m_xmf3Up.y;
	m_xmf4x4WorldMatrix._23 = m_xmf3Up.z;
	m_xmf4x4WorldMatrix._31 = m_xmf3Look.x;
	m_xmf4x4WorldMatrix._32 = m_xmf3Look.y;
	m_xmf4x4WorldMatrix._33 = m_xmf3Look.z;
	m_xmf4x4WorldMatrix._41 = m_xmf3Position.x;
	m_xmf4x4WorldMatrix._42 = m_xmf3Position.y;
	m_xmf4x4WorldMatrix._43 = m_xmf3Position.z;
}

void CPlayer::Animate(float fElapsedTime)
{
	OnUpdateTransform();
	CGameObject::Animate(fElapsedTime);
}

CAirplanePlayer::CAirplanePlayer()
{
}

CAirplanePlayer::~CAirplanePlayer()
{
}

void CAirplanePlayer::OnUpdateTransform()
{
	CPlayer::OnUpdateTransform();

	m_xmf4x4WorldMatrix = Matrix4x4::Multiply(Matrix4x4::RotationRollPitchYaw(90.f, 0.f, 0.f), m_xmf4x4WorldMatrix);
}
