#include "GameObject.h"
#include "GraphicsPipeline.h"
#include "MyMath.h"


CGameObject::~CGameObject()
{
	//	게임 객체 인스턴스가 메쉬를 참조하지 않기 때문에 참조값을 1 감소
	if (m_pMesh) m_pMesh->Release();
}

void CGameObject::SetPosition(float x, float y, float z)
{
	m_xmf4x4WorldMatrix._41 = x;
	m_xmf4x4WorldMatrix._42 = y;
	m_xmf4x4WorldMatrix._43 = z;
}

void CGameObject::SetPosition(const XMFLOAT3& xmf3Position)
{
	m_xmf4x4WorldMatrix._41 = xmf3Position.x;
	m_xmf4x4WorldMatrix._42 = xmf3Position.y;
	m_xmf4x4WorldMatrix._43 = xmf3Position.z;
}

void CGameObject::SetRotationAxis(const XMFLOAT3& xmf3RotationAxis)
{
	m_xmf3RotationAxis = Vector3::Normalize(xmf3RotationAxis);
}

void CGameObject::SetMovingDirection(const XMFLOAT3& xmf3MovingDirection)
{
	m_xmf3MovingDirection = Vector3::Normalize(xmf3MovingDirection);
}

void CGameObject::Move(const XMFLOAT3& vDirection, float fSpeed)
{
	XMFLOAT3 xmf3Position = XMFLOAT3(
		m_xmf4x4WorldMatrix._41, m_xmf4x4WorldMatrix._42, m_xmf4x4WorldMatrix._43
	);
	xmf3Position = Vector3::Add(xmf3Position, Vector3::ScalarProduct(vDirection, fSpeed));
	SetPosition(xmf3Position);

}

void CGameObject::Rotate(float fPitch, float fYaw, float fRoll)
{
	XMFLOAT4X4 xmf4x4RotationMatrix = Matrix4x4::RotationRollPitchYaw(fPitch, fYaw, fRoll);
	m_xmf4x4WorldMatrix = Matrix4x4::Multiply(xmf4x4RotationMatrix, m_xmf4x4WorldMatrix);
}

void CGameObject::Rotate(const XMFLOAT3& xmf3Axis, float fAngle)
{
	XMFLOAT4X4 xmf4x4RotationMatrix = Matrix4x4::RotationAxis(xmf3Axis, fAngle);
	m_xmf4x4WorldMatrix = Matrix4x4::Multiply(xmf4x4RotationMatrix, m_xmf4x4WorldMatrix);
}

void CGameObject::Render(HDC hDCFrameBuffer, CCamera* pCamera)
{
	if (m_pMesh)
	{
		CGraphicsPipeline::SetWorldTransform(&m_xmf4x4WorldMatrix);
		HPEN hPen = ::CreatePen(PS_SOLID, 0, m_dwColor);
		HPEN hOldPen = (HPEN)::SelectObject(hDCFrameBuffer, hPen);
		m_pMesh->Render(hDCFrameBuffer);
		::SelectObject(hDCFrameBuffer, hOldPen);
		::DeleteObject(hPen);
	}
}

void CGameObject::Animate(float fElapsedTime)
{
	//	자전
	if (!isZero(m_fRotationSpeed)) Rotate(m_xmf3RotationAxis, m_fRotationSpeed * fElapsedTime);
	//	평행이동
	if (!isZero(m_fMovingSpeed)) Move(m_xmf3MovingDirection, m_fMovingSpeed * fElapsedTime);
	//	칼라 애니메이션
	//	if (m_bColorAnimation) AnimateColor(fElapsedTime);
}

void CGameObject::AnimateColor(float fElapsedTime)
{
	XMFLOAT3 colorVector = Vector3::Add(m_xmf3color, Vector3::ScalarProduct(m_xmf3colorSpeed, fElapsedTime));

	if (int(colorVector.x) > 255) {
		colorVector.x = 0.f;
	}
	if (int(colorVector.y) > 255) {
		colorVector.y = 0.f;
	}
	if (int(colorVector.z) > 255) {
		colorVector.z = 0.f;
	}
	m_dwColor = RGB(int(colorVector.x), int(colorVector.y), int(colorVector.z));
}