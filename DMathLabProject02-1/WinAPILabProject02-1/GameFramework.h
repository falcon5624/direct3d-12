#pragma once
#include "Player.h"
#include "Scene.h"
#include "GameTimer.h"

class CGameFramework
{
private:
	/* window API */
	HINSTANCE m_hInstance = NULL;			//	응용 프로그램의 인스턴스 핸들
	HWND m_hWnd = NULL;						//	주 윈도우 핸들
	RECT m_rcClient;						//	주 윈도우의 클라이언트 사각형
	HDC m_hDCFrameBuffer = NULL;			//	렌더링 대상이 되는 프레임 버퍼
	HBITMAP m_hBitmapFrameBuffer = NULL;	//	렌더링된 비트맵
	HBITMAP m_hBitmapSelect = NULL;			//	비트맵 디바이스 컨텍스트

	/* Game software */
	CPlayer* m_pPlayer = NULL;				//	플레이어 객체
	CScene* m_pScene = NULL;				//	게임 세계의 장면
	CGameTimer m_GameTimer;					//	프레임 레이트를 관리하기 위한 객체

	POINT m_ptOldCursorPos;					//	마지막 마우스 버튼 클릭 때의 마우스 커서 위치
	_TCHAR m_pszFrameRate[50];				//	프레임 레이트 출력을 위한 문자열

public:
	CGameFramework() { }
	~CGameFramework() { }

	//	프레임워크를 생성하는 함수(주 윈도우가 생성되면 호출된다)
	void OnCreate(HINSTANCE hInstance, HWND hMainWnd);
	//	프레임워크를 소멸하는 함수(응용프로그램이 종료되면 호출된다)
	void OnDestroy();

	//	게임 세계를 렌더링할 비트맵 표면을 생성
	void BuildFrameBuffer();
	//	게임 세계를 렌더링할 비트맵 표면을 삭제
	void ClearFrameBuffer(DWORD dwColor);
	//	게임 세계를 렌더링할 비트맵 표면을 클라이언트 영역으로 복사
	void PresentFrameBuffer();

	//	객체들 렌더링
	void BuildObjects();
	//	객체들 소멸
	void ReleaseObjects();

	/*	프레임워크의 핵심 */
	//	사용자 입력 처리 함수
	void ProcessInput();
	//	애니메이션 함수
	void AnimateObjects();
	//	프레임 처리
	void FrameAdvance();

	/* 윈도우 메세지 처리(키보드 & 마우스) */
	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM
		wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM
		wParam, LPARAM lParam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT
		nMessageID, WPARAM wParam, LPARAM lParam);
};

