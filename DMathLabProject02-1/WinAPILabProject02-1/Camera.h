#pragma once
#include "Mesh.h"

class CViewport {
public:
	int m_nLeft;
	int m_nTop;
	int m_nWidth;
	int m_nHeight;

	CViewport() { }
	virtual ~CViewport() { }

	void SetViewport(int nLeft, int nTop, int nWidth, int nHeight) { m_nLeft = nLeft; m_nTop = nTop; m_nWidth = nWidth; m_nHeight = nHeight; }
};

class CPlayer;

class CCamera
{
private:
	XMFLOAT3 m_xmf3Position = XMFLOAT3(0.f, 0.f, 0.f);
	XMFLOAT3 m_xmf3Look = XMFLOAT3(0.f, 0.f, 1.f);
	XMFLOAT3 m_xmf3Up = XMFLOAT3(0.f, 1.f, 0.f);
	XMFLOAT3 m_xmf3Right = XMFLOAT3(1.f, 0.f, 0.f);

	float m_fFOVAngle = 90.f;					//	시야각
	float m_fProjectRectDistance = 1.f;			//	투영 사각형까지의 거리
	float m_fAspectRatio = float(FRAMEBUFF_WIDTH) / float(FRAMEBUFF_HEIGHT);	//	종횡비

	XMFLOAT4X4 m_xmf4x4ViewMatrix = Matrix4x4::MakeIdentity();
	XMFLOAT4X4 m_xmf4x4ProjectionMatrix = Matrix4x4::MakeIdentity();
	XMFLOAT4X4 m_xmf4x4ViewProjection = Matrix4x4::MakeIdentity();

public:
	CViewport m_Viewport;						//	뷰포트

	CCamera();
	~CCamera();

	/* Set */

	void SetViewMatrix();
	void GenerateViewMatrix();
	void SetProjectionMatrix(float fNearPlaneDistance, float fFarPlaneDistance, float fFOVAngle);
	void SetViewport(int xStart, int yStart, int nWidth, int nHeight);
	void SetFOVAngle(float FOVAngle);
	void SetLookAtWithPosition(XMFLOAT3& xmf3Position, XMFLOAT3& xmf3LookAt, XMFLOAT3& xmf3Up);
	void SetLookAt(XMFLOAT3& xmf3LookAt, XMFLOAT3& xmf3Up);

	/* Get */
	XMFLOAT4X4* GetViewProjectionMatrix() { return &m_xmf4x4ViewMatrix; }

	//////////////////////////////////////////////////////////////////

	/* Action */

	void Move(const XMFLOAT3& xmf3Shift);
	void Move(float x, float y, float z);

	void Rotate(float fPitch = 0.0f, float fYaw = 0.0f, float fRoll =
		0.0f);

	void Update(CPlayer* pPlayer, XMFLOAT3& xmf3LookAt, float
		fTimeElapsed = 0.016f);
};

