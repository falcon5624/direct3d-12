#include "Camera.h"
#include "Player.h"

CCamera::CCamera()
{
}

CCamera::~CCamera()
{
}

void CCamera::SetViewMatrix()
{
	m_xmf4x4ViewMatrix._11 = m_xmf3Right.x; 
	m_xmf4x4ViewMatrix._12 = m_xmf3Up.x;
	m_xmf4x4ViewMatrix._13 = m_xmf3Look.x;

	m_xmf4x4ViewMatrix._21 = m_xmf3Right.y; 
	m_xmf4x4ViewMatrix._22 = m_xmf3Up.y;
	m_xmf4x4ViewMatrix._23 = m_xmf3Look.y;

	m_xmf4x4ViewMatrix._31 = m_xmf3Right.z; 
	m_xmf4x4ViewMatrix._32 = m_xmf3Up.z;
	m_xmf4x4ViewMatrix._33 = m_xmf3Look.z;

	m_xmf4x4ViewMatrix._41 = -Vector3::DotProduct(m_xmf3Position, m_xmf3Right);
	m_xmf4x4ViewMatrix._42 = -Vector3::DotProduct(m_xmf3Position, m_xmf3Up);
	m_xmf4x4ViewMatrix._43 = -Vector3::DotProduct(m_xmf3Position, m_xmf3Look);
}

void CCamera::GenerateViewMatrix()
{
	/* 외적을 이용한 look, up, right 직교화 함수 - 부동소수점 오류 문제 */
	m_xmf3Look = Vector3::Normalize(m_xmf3Look);
	m_xmf3Up = Vector3::Normalize(m_xmf3Up);
	m_xmf3Right = Vector3::Normalize(Vector3::CrossProduct(m_xmf3Up, m_xmf3Look));
	m_xmf3Up = Vector3::Normalize(Vector3::CrossProduct(m_xmf3Look, m_xmf3Right));

	SetViewMatrix();

	m_xmf4x4ViewProjection = Matrix4x4::Multiply(m_xmf4x4ViewMatrix, m_xmf4x4ProjectionMatrix);
}

void CCamera::SetProjectionMatrix(float fNearPlaneDistance, float fFarPlaneDistance, float fFOVAngle)
{
	m_fAspectRatio = float(m_Viewport.m_nWidth) / float(m_Viewport.m_nHeight);
	m_xmf4x4ProjectionMatrix = Matrix4x4::PerspectiveForLH(fFOVAngle, m_fAspectRatio, fNearPlaneDistance, fFarPlaneDistance);
}

void CCamera::SetViewport(int nLeft, int nTop, int nWidth, int nHeight)
{
	m_Viewport.SetViewport(nLeft, nTop, nWidth, nHeight);
	m_fAspectRatio = float(m_Viewport.m_nWidth) /
		float(m_Viewport.m_nHeight);
}

void CCamera::SetFOVAngle(float FOVAngle)
{
	m_fFOVAngle = FOVAngle;
	m_fProjectRectDistance = 1.f / tanf(XMConvertToRadians(FOVAngle * 0.5f));
}

void CCamera::SetLookAtWithPosition(XMFLOAT3& xmf3Position, XMFLOAT3& xmf3LookAt, XMFLOAT3& xmf3Up)
{
	m_xmf3Position = xmf3Position;
	m_xmf4x4ViewMatrix = Matrix4x4::LookAtLH(xmf3Position, xmf3LookAt, xmf3Up);

	m_xmf3Look = Vector3::Normalize(XMFLOAT3(m_xmf4x4ViewMatrix._11, m_xmf4x4ViewMatrix._21, m_xmf4x4ViewMatrix._31));
	m_xmf3Up = Vector3::Normalize(XMFLOAT3(m_xmf4x4ViewMatrix._12, m_xmf4x4ViewMatrix._22, m_xmf4x4ViewMatrix._32));
	m_xmf3Right = Vector3::Normalize(XMFLOAT3(m_xmf4x4ViewMatrix._13, m_xmf4x4ViewMatrix._23, m_xmf4x4ViewMatrix._33));
}

void CCamera::SetLookAt(XMFLOAT3& xmf3LookAt, XMFLOAT3& xmf3Up)
{
	/* 현재 카메라의 위치에서 플레이어를 바라보기 위한 카메라 변환 행렬 */
	this->SetLookAtWithPosition(m_xmf3Position, xmf3LookAt, xmf3Up);
}

void CCamera::Move(const XMFLOAT3& xmf3Shift)
{
	m_xmf3Position = Vector3::Add(m_xmf3Position, xmf3Shift);
}

void CCamera::Move(float x, float y, float z)
{
	this->Move(XMFLOAT3(x, y, z));
}

void CCamera::Rotate(float fPitch, float fYaw, float fRoll)
{
	if (!isZero(fPitch)) {
		XMFLOAT4X4 xmf4x4RotationMatrix =
		Matrix4x4::RotationAxis(m_xmf3Right, fPitch);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4RotationMatrix);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4RotationMatrix);
	}
	if (!isZero(fYaw)) {
		XMFLOAT4X4 xmf4x4RotationMatrix =
		Matrix4x4::RotationAxis(m_xmf3Up, fYaw);
		m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmf4x4RotationMatrix);
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4RotationMatrix);
	}
	if (!isZero(fRoll)) {
		XMFLOAT4X4 xmf4x4RotationMatrix =
		Matrix4x4::RotationAxis(m_xmf3Look, fRoll);
		m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmf4x4RotationMatrix);
		m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmf4x4RotationMatrix);
	}
}

void CCamera::Update(CPlayer* pPlayer, XMFLOAT3& xmf3LookAt, float fTimeElapsed)
{
	/* 카메라의 이동, 회전에 따라 카메라의 정보를 갱신하는 가상함수 */

	XMFLOAT4X4 xmf4x4RotationMatrix = Matrix4x4::MakeIdentity();
	xmf4x4RotationMatrix._11 = pPlayer->m_xmf3Right.x;
	xmf4x4RotationMatrix._12 = pPlayer->m_xmf3Right.y;
	xmf4x4RotationMatrix._13 = pPlayer->m_xmf3Right.z;

	xmf4x4RotationMatrix._21 = pPlayer->m_xmf3Up.x;
	xmf4x4RotationMatrix._22 = pPlayer->m_xmf3Up.y;
	xmf4x4RotationMatrix._23 = pPlayer->m_xmf3Up.z;

	xmf4x4RotationMatrix._31 = pPlayer->m_xmf3Look.x;
	xmf4x4RotationMatrix._32 = pPlayer->m_xmf3Look.y;
	xmf4x4RotationMatrix._33 = pPlayer->m_xmf3Look.z;

	XMFLOAT3 xmf3CameraOffset = Vector3::TransformCoord(pPlayer->m_xmf3CameraOffset, xmf4x4RotationMatrix);
	XMFLOAT3 xmf3NewPosition = Vector3::Add(pPlayer->m_xmf3Position, xmf3CameraOffset);
	XMFLOAT3 xmfDirection = Vector3::Subtract(xmf3NewPosition, m_xmf3Position); // 회전된 위치 - 현재 카메라의 위치
	float fLength = Vector3::GetLength(xmfDirection);
	xmfDirection = Vector3::Normalize(xmfDirection);

	float fDelayScale = fTimeElapsed * 4.f;
	float fDistance = fLength * fDelayScale;

	if (fDistance > fLength) fDistance = fLength;
	if (fLength < 0.01f) fDistance = fLength;
	if (fDistance > 0)
	{
		//	카메라를 공전하지 않고 이동을 한다(회전의 각도가 작은 경우 회전 이동은 선형 이동과 거의 같다).
		m_xmf3Position = Vector3::Add(m_xmf3Position, Vector3::ScalarProduct(xmfDirection, fDistance));
		//	카메라가 플레이어를 바라보도록 한다.
		SetLookAt(pPlayer->m_xmf3Position, pPlayer->m_xmf3Up);
	}
}

