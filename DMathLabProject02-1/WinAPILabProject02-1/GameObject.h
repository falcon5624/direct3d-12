#pragma once
#include "Mesh.h"
#include "Camera.h"

class CGameObject
{
public:
	//	객체의 월드변환 행렬
	XMFLOAT4X4 m_xmf4x4WorldMatrix = Matrix4x4::MakeIdentity();

	/* 물리 */
	//	게임 객체의 회전축
	XMFLOAT3 m_xmf3RotationAxis = XMFLOAT3(0.f, 1.f, 0.f);	
	float m_fRotationSpeed = 0.f;
	XMFLOAT3 m_xmf3MovingDirection = XMFLOAT3(0.f, 0.f, 1.f);
	float m_fMovingSpeed = 0.f;
	float m_fMovingRange = 0.f;

	/* 렌더 */
	//	게임 객체의 메쉬
	CMesh* m_pMesh = NULL;
	//	게임 객체의 색상(선분의 색상)
	DWORD m_dwColor = RGB(0, 255, 255);
	//	게임 객체의 색상 Float 형태(애니메이션을 위함)
	XMFLOAT3 m_xmf3color;
	//	색상의 변화 속도
	XMFLOAT3 m_xmf3colorSpeed;

	bool m_bActive = true;
	bool m_bColorAnimation = false;

public:
	CGameObject() { }
	~CGameObject();

	/* Set */
	void SetMesh(CMesh* pMesh) { m_pMesh = pMesh; if (pMesh) pMesh->AddRef(); }
	void SetActive(bool bActive) { m_bActive = bActive; }
	void SetColor(DWORD dwColor) { m_dwColor = dwColor; m_xmf3color.x = float(GetRValue(m_dwColor)); m_xmf3color.y = float(GetGValue(m_dwColor)); m_xmf3color.z = float(GetBValue(m_dwColor)); }
	void SetColorSpeed(float fRedSpeed, float fGreenSpeed, float fBlueSpeed) { m_xmf3colorSpeed.x = fRedSpeed; m_xmf3colorSpeed.y = fGreenSpeed; m_xmf3colorSpeed.z = fBlueSpeed; }
	void SetPosition(float x, float y, float z);
	void SetPosition(const XMFLOAT3& xmf3Position);
	void SetMovingDirection(const XMFLOAT3& xmf3MovingDirection);
	void SetMovingSpeed(float fSpeed) { m_fMovingSpeed = fSpeed; }
	void SetMovingRange(float fRange) { m_fMovingRange = fRange; }
	void SetRotationAxis(const XMFLOAT3& xmf3RotationAxis);
	void SetRotationSpeed(float fSpeed) { m_fRotationSpeed = fSpeed; }

	/* Action */
	void Move(const XMFLOAT3& vDirection, float fSpeed);
	void Rotate(float fPitch, float fYaw, float fRoll);
	void Rotate(const XMFLOAT3& xmf3Axis, float fAngle);

	virtual void OnUpdateTransform() { };
	virtual void Render(HDC hDCFrameBuffer, CCamera* pCamera);

	/* Animation */
	virtual void Animate(float fElapsedTime);
	void AnimateColor(float fElapsedTime);
};

