#pragma once
#include "GameObject.h"
#include "Player.h"

class CScene
{
private:
	UINT m_nObjects = 0;				//	���� ��ü���� ����
	CGameObject** m_ppObjects = NULL;	//	��ü���� �迭
	CPlayer* m_pPlayer = NULL;

public:
	CScene(CPlayer* pPlayer) { m_pPlayer = pPlayer; }
	virtual ~CScene() { }

	//	��ü�� ����
	virtual void BuildObjects();
	//	��ü�� �Ҹ�
	virtual void ReleaseObjects();
	//	��ü�� �ִϸ��̼�
	virtual void Animate(float fElapsedTime);
	//	��ü�� ������
	virtual void Render(HDC hDCFrameBuffer, CCamera* pCamera);

	virtual void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID,
		WPARAM wParam, LPARAM lParam) { }
	virtual void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID,
		WPARAM wParam, LPARAM lParam) { }
};

