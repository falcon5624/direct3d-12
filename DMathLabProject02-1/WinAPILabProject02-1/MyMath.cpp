#include "stdafx.h"
#include "Mesh.h"
#include "MyMath.h"

bool isZero(float number)
{
	return ((-FLT_EPSILON < number) && (number < FLT_EPSILON));
}

bool isEqual(float lhs, float rhs)
{
	return isZero(lhs - rhs);
}

XMFLOAT4X4 Matrix4x4::MakeIdentity()
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixIdentity());
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::Multiply(const XMFLOAT4X4& xmf4x4_lhs, const XMFLOAT4X4& xmf4x4_rhs)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMLoadFloat4x4(&xmf4x4_lhs) * XMLoadFloat4x4(&xmf4x4_rhs));
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::Inverse(const XMFLOAT4X4& xmf4x4)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixInverse(nullptr, XMLoadFloat4x4(&xmf4x4)));
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::Transpose(const XMFLOAT4X4& xmf4x4)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4)));
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::RotationRollPitchYaw(float fPitch, float fYaw, float fRoll)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixRotationRollPitchYaw(
		XMConvertToRadians(fPitch),
		XMConvertToRadians(fYaw),
		XMConvertToRadians(fRoll)));
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::RotationAxis(const XMFLOAT3& xmfAxis, float fAngle)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixRotationAxis(XMLoadFloat3(&xmfAxis), XMConvertToRadians(fAngle)));
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::PerspectiveForLH(float fFovy, float fAspect, float fNear, float fFar)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixPerspectiveFovLH(XMConvertToRadians(fFovy), fAspect, fNear, fFar));
	return xmf4x4ReturnValue;
}

XMFLOAT4X4 Matrix4x4::LookAtLH(const XMFLOAT3& xmf3Eye, const XMFLOAT3& xmf3Focus, const XMFLOAT3& xmf3Up)
{
	XMFLOAT4X4 xmf4x4ReturnValue;
	XMStoreFloat4x4(&xmf4x4ReturnValue, XMMatrixLookAtLH(
		XMLoadFloat3(&xmf3Eye),
		XMLoadFloat3(&xmf3Focus),
		XMLoadFloat3(&xmf3Up)
		));
	return xmf4x4ReturnValue;
}

XMFLOAT3 Vector3::Add(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMLoadFloat3(&xmf3_lhs) + XMLoadFloat3(&xmf3_rhs));
	return xmf3ReturnValue;
}

XMFLOAT3 Vector3::Subtract(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMLoadFloat3(&xmf3_lhs) - XMLoadFloat3(&xmf3_rhs));
	return xmf3ReturnValue;
}

XMFLOAT3 Vector3::ScalarProduct(const XMFLOAT3& xmf3, float fScalar)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMLoadFloat3(&xmf3) * fScalar);
	return xmf3ReturnValue;
}

float Vector3::DotProduct(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3Dot(XMLoadFloat3(&xmf3_lhs), XMLoadFloat3(&xmf3_rhs)));
	return xmf3ReturnValue.x;
}

XMFLOAT3 Vector3::CrossProduct(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3Cross(XMLoadFloat3(&xmf3_lhs), XMLoadFloat3(&xmf3_rhs)));
	return xmf3ReturnValue;
}

XMFLOAT3 Vector3::Normalize(const XMFLOAT3& xmf3)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3Normalize(XMLoadFloat3(&xmf3)));
	return xmf3ReturnValue;
}

XMFLOAT3 Vector3::TransformNormal(const XMFLOAT3 xmf3, const XMFLOAT4X4 xmf4x4)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3TransformNormal(XMLoadFloat3(&xmf3), XMLoadFloat4x4(&xmf4x4)));
	return xmf3ReturnValue;
}

XMFLOAT3 Vector3::TransformCoord(const XMFLOAT3 xmf3, const XMFLOAT4X4 xmf4x4)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3TransformCoord(XMLoadFloat3(&xmf3), XMLoadFloat4x4(&xmf4x4)));
	return xmf3ReturnValue;
}

float Vector3::GetLength(const XMFLOAT3& xmf3)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3Length(XMLoadFloat3(&xmf3)));
	return xmf3ReturnValue.x;
}

float Vector3::GetDistance(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVector3Length(XMLoadFloat3(&xmf3_lhs) - XMLoadFloat3(&xmf3_rhs)));
	return xmf3ReturnValue.x;
}

float Vector3::GetAngle(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue;
	XMStoreFloat3(&xmf3ReturnValue, XMVectorACos(XMVector3AngleBetweenNormals(XMLoadFloat3(&xmf3_lhs), XMLoadFloat3(&xmf3_rhs))));
	return XMConvertToDegrees(xmf3ReturnValue.x);
}

bool Vector3::isEqual(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs)
{
	XMFLOAT3 xmf3ReturnValue = Vector3::Subtract(xmf3_lhs, xmf3_rhs);
	return (isZeroVector(xmf3ReturnValue));
}

bool Vector3::isZeroVector(const XMFLOAT3& xmf3)
{
	return ((isZero(xmf3.x)) && (isZero(xmf3.y)) && (isZero(xmf3.z)));
}
