#include "stdafx.h"
#include "GraphicsPipeline.h"

XMFLOAT4X4* CGraphicsPipeline::m_pxmf4x4WorldMatrix = NULL;
XMFLOAT4X4* CGraphicsPipeline::m_pxmf4x4ViewProjectionMatrix = NULL;
CViewport* CGraphicsPipeline::m_pViewport = NULL;

XMFLOAT3 CGraphicsPipeline::ScreenTransform(XMFLOAT3& xmf3Project)
{
	XMFLOAT3 xmf3ScreenPoint = xmf3Project;

	float fHalfWidth = m_pViewport->m_nWidth * 0.5f;
	float fHalfHeight = m_pViewport->m_nHeight * 0.5f;
	xmf3ScreenPoint.x = m_pViewport->m_nLeft + (xmf3Project.x * fHalfWidth) + fHalfWidth;
	xmf3ScreenPoint.y = m_pViewport->m_nTop + (-xmf3Project.y * fHalfHeight) + fHalfHeight;

	return xmf3ScreenPoint;
}

XMFLOAT3 CGraphicsPipeline::Project(XMFLOAT3& xmf3Model)
{
	XMFLOAT4X4 xmf4x4ModelToProject = Matrix4x4::Multiply(*m_pxmf4x4WorldMatrix, *m_pxmf4x4ViewProjectionMatrix);
	XMFLOAT3 xmf3ProjectPoint = Vector3::TransformCoord(xmf3Model, xmf4x4ModelToProject);
	return xmf3ProjectPoint;
}
