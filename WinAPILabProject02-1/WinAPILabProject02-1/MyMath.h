#pragma once
#include <cfloat>

bool isZero(float number);
bool isEqual(float lhs, float rhs);

#define DegreeToRadian(x)		float((x) * 3.141592654f / 180.f)

#define GetRValue(rgb)      ((BYTE)(rgb))
#define GetGValue(rgb)      ((BYTE)(((WORD)(rgb)) >> 8))
#define GetBValue(rgb)      ((BYTE)((rgb)>>16))
