#include "Camera.h"
#include "MyMath.h"

CPoint3D CCamera::CameraTransform(CPoint3D& f3World)
{ 
	//	카메라를 월드 좌표계의 원점으로 이동
	CPoint3D f3Camera = f3World;
	f3Camera.x -= m_fposX;
	f3Camera.y -= m_fposY;
	f3Camera.z -= m_fposZ;

	float fPitch = DegreeToRadian(-m_fpitch);
	float fYaw = DegreeToRadian(-m_fyaw);
	float fRoll = DegreeToRadian(-m_froll);

	//	카메라를 월드 좌표계의 축과 일치하도록 회전
	CPoint3D f3Rotated = f3Camera;
	if (!isZero(fPitch)) {
		f3Rotated.y = f3Camera.y * cosf(fPitch) - f3Camera.z * sinf(fPitch);
		f3Rotated.z = f3Camera.y * sinf(fPitch) + f3Camera.z * cosf(fPitch);
		f3Camera.y = f3Rotated.y;
		f3Camera.z = f3Rotated.z;
	}
	if (!isZero(fYaw)) {
		f3Rotated.x = f3Camera.x * cosf(fYaw) + f3Camera.z * sinf(fYaw);
		f3Rotated.z = -f3Camera.x * sinf(fYaw) + f3Camera.z * cosf(fYaw);
		f3Camera.x = f3Rotated.x;
		f3Camera.z = f3Rotated.z;
	}
	if (!isZero(fRoll)) {
		f3Rotated.x = f3Camera.x * cosf(fRoll) - f3Camera.y * sinf(fRoll);
		f3Rotated.y = f3Camera.x * sinf(fRoll) + f3Camera.y * cosf(fRoll);
		f3Camera.x = f3Rotated.x;
		f3Camera.y = f3Rotated.y;
	}

	return f3Camera;
}

CPoint3D CCamera::ProjectionTransform(CPoint3D& f3Camera)
{
	CPoint3D f3Project = f3Camera;
	if (!isZero(f3Camera.z)) {
		//	카메라의 시야각이 90'가 아닐 경우 투영 사각형까지의 거리를 곱한다. 
		f3Project.x = f3Camera.x * m_fProjectRectDistance / (m_AspectRatio * f3Camera.z);
		f3Project.y = f3Camera.y * m_fProjectRectDistance / (f3Camera.z);

		//	투영 좌표계는 2차원이므로 z 좌표에 카메라 좌표계의 z를 저장한다.
		f3Project.z = f3Camera.z;
	}
	return f3Project;
}

CPoint3D CCamera::ScreenTransform(CPoint3D& f3Projection)
{
	CPoint3D f3Screen = f3Projection;
	float fHalfWidth = float(m_pViewport->m_nWidth) * 0.5f;
	float fHalfHeight = float(m_pViewport->m_nHeight) * 0.5f;
	f3Screen.x = (f3Projection.x * fHalfWidth) + float(m_pViewport->m_nLeft)
		+ fHalfWidth;	
	f3Screen.y = (-f3Projection.y * fHalfHeight)
		+ float(m_pViewport->m_nTop) + fHalfHeight;
	return f3Screen;
}

void CCamera::SetViewport(int xStart, int yStart, int nWidth, int nHeight)
{
	m_pViewport = new CViewport(xStart, yStart, nWidth, nHeight);
	m_AspectRatio = float(m_pViewport->m_nWidth) / float(m_pViewport->m_nHeight);
}

void CCamera::SetFOVAngle(float FOVAngle)
{
	m_fFOVAngle = FOVAngle;
	m_fProjectRectDistance = 1.f / tanf(DegreeToRadian(FOVAngle * 0.5f));
}

