#pragma once
#include "GameObject.h"
#include "Camera.h"

class CPlayer : public CGameObject
{
private:
	CCamera* m_pCamera = NULL;

public:
	CPlayer() { m_pCamera = new CCamera(); }
	virtual ~CPlayer() { if (m_pCamera) delete m_pCamera; }

	void SetPosition(float x, float y, float z);
	void SetRotation(float pitch, float yaw, float roll);

	void Move(float x, float y, float z);
	void Rotate(float x, float y, float z);

	void SetCamera(CCamera* pCamera) { m_pCamera = pCamera; }
	CCamera* GetCamera() { return m_pCamera; }
};

