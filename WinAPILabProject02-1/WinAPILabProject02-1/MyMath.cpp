#include "stdafx.h"
#include "Mesh.h"
#include "MyMath.h"

bool isZero(float number)
{
	return ((-FLT_EPSILON < number) && (number < FLT_EPSILON));
}

bool isEqual(float lhs, float rhs)
{
	return isZero(lhs - rhs);
}
