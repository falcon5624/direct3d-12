#include "Mesh.h"
#include "GraphicsPipeline.h"

void Draw2DLine(HDC hDCFrameBuffer, CPoint3D& f3PreviousProject, CPoint3D& f3CurrentProject)
{
	//	투영 좌표계의 두 점을 화면 좌표계로 변환해 픽셀로 연결된 선분으로 그린다.
	CPoint3D f3Previous = CGraphicsPipeline::ScreenTransform(f3PreviousProject);
	CPoint3D f3Current = CGraphicsPipeline::ScreenTransform(f3CurrentProject);
	::MoveToEx(hDCFrameBuffer, (long)f3Previous.x, (long)f3Previous.y, NULL);
	::LineTo(hDCFrameBuffer, (long)f3Current.x, (long)f3Current.y);
}

CPolygon::CPolygon(UINT nVertices)
{
	m_nVertices = nVertices;
	m_pVertices = new CVertex[nVertices];
}

CPolygon::~CPolygon()
{
	if (m_pVertices) delete[] m_pVertices;
}

void CPolygon::SetVertex(int nIndex, CVertex vertex)
{
	if ((0 <= nIndex) && (nIndex < m_nVertices) && m_pVertices) {
		m_pVertices[nIndex] = vertex;
	}
}

CMesh::CMesh(UINT nPolygons)
{
	m_nPolygons = nPolygons;
	m_ppPolygons = new CPolygon * [nPolygons];
}

CMesh::~CMesh()
{
	if (m_ppPolygons) {
		for (UINT i = 0; i < m_nPolygons; ++i) {
			if (m_ppPolygons) delete m_ppPolygons[i];
		}
		delete[] m_ppPolygons;
	}
}

void CMesh::SetPolygon(int nIndex, CPolygon* pPolygon)
{
	//	메쉬의 다각형 설정
	if ((0 <= nIndex) && (nIndex < m_nPolygons)) m_ppPolygons[nIndex] = pPolygon;
}

void CMesh::Render(HDC hDCFrameBuffer)
{
	CPoint3D f3InitialProject, f3PreviousProject, f3Intersect;
	bool bPreviousInside = false, bInitialInside = false, bCurrentInside = false, bIntersectInside = false;

	//	메쉬를 구성하는 모든 다각형들을 렌더링
	for (UINT j = 0; j < m_nPolygons; ++j) {
		UINT nVertices = m_ppPolygons[j]->m_nVertices;
		CVertex* pVertices = m_ppPolygons[j]->m_pVertices;

		//	폴리곤의 첫 번째 정점을 원근 투영 변환한다.
		f3PreviousProject = f3InitialProject = CGraphicsPipeline::Project(pVertices[0].m_f3Pos);
		//	변환된 점이 투영 사각형 안에 포함되는가?
		bPreviousInside = bInitialInside = (-1.f <= f3InitialProject.x)
			&& (f3InitialProject.x <= 1.f)
			&& (-1.f <= f3InitialProject.y)
			&& (f3InitialProject.y <= 1.f);

		//	폴리곤을 구성하는 모든 정점들을 원근 투영 변환하고 선분으로 렌더링
		for (UINT i = 1; i < nVertices; ++i) {
			CPoint3D f3CurrentProject = CGraphicsPipeline::Project(pVertices[i].m_f3Pos);
			//	변환된 점들이 투영 사각형 안에 포함되는가?
			bCurrentInside = (-1.f <= f3CurrentProject.x)
				&& (f3CurrentProject.x <= 1.f)
				&& (-1.f <= f3CurrentProject.y)
				&& (f3CurrentProject.y <= 1.f);
			//	투영 사각형 안에 있다면 현재 점을 선분으로 그린다.
			if (((f3PreviousProject.z >= 0.f) || (f3CurrentProject.z >= 0.f))
				&& ((bCurrentInside || bPreviousInside))) {
				::Draw2DLine(hDCFrameBuffer, f3PreviousProject, f3CurrentProject);
				f3PreviousProject = f3CurrentProject;
				bPreviousInside = bCurrentInside;
			}
		}

		//	폴리곤의 마지막 정점과 시작점을 선분으로 그린다.
		if (((f3PreviousProject.z >= 0.f) || (f3InitialProject.z >= 0.f)) && ((bInitialInside || bPreviousInside))) {
			::Draw2DLine(hDCFrameBuffer, f3PreviousProject, f3InitialProject);
		}
	}
}

CCubeMesh::CCubeMesh(float fWidth, float fHeight, float fDepth) : CMesh(6)
{
	/* 0, 0, 0 모델 좌표계 원점 기준
		width	x축 변
		height	y축 변
		depth	z축 변

		winding order - CW (LHS)
	*/

	CPolygon* forwardFace = new CPolygon(4);	//	전
	CPolygon* leftFace = new CPolygon(4);		//	좌
	CPolygon* rightFace = new CPolygon(4);		//	우
	CPolygon* backwardFace = new CPolygon(4);	//	후
	CPolygon* upFace = new CPolygon(4);			//	상
	CPolygon* downFace = new CPolygon(4);		//	하

	forwardFace->SetVertex(0, CVertex(-fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	forwardFace->SetVertex(1, CVertex(fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	forwardFace->SetVertex(2, CVertex(fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	forwardFace->SetVertex(3, CVertex(-fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));

	leftFace->SetVertex(0, CVertex(-fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	leftFace->SetVertex(1, CVertex(-fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	leftFace->SetVertex(2, CVertex(-fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	leftFace->SetVertex(3, CVertex(-fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));

	rightFace->SetVertex(0, CVertex(fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	rightFace->SetVertex(1, CVertex(fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	rightFace->SetVertex(2, CVertex(fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	rightFace->SetVertex(3, CVertex(fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));

	backwardFace->SetVertex(0, CVertex(fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	backwardFace->SetVertex(1, CVertex(-fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	backwardFace->SetVertex(2, CVertex(-fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	backwardFace->SetVertex(3, CVertex(fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));

	upFace->SetVertex(0, CVertex(-fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	upFace->SetVertex(1, CVertex(-fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	upFace->SetVertex(2, CVertex(fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	upFace->SetVertex(3, CVertex(fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));

	downFace->SetVertex(0, CVertex(fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	downFace->SetVertex(1, CVertex(fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	downFace->SetVertex(2, CVertex(-fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	downFace->SetVertex(3, CVertex(-fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));

	
	SetPolygon(0, forwardFace);
	SetPolygon(1, backwardFace);
	SetPolygon(2, leftFace);
	SetPolygon(3, rightFace);
	SetPolygon(4, upFace);
	SetPolygon(5, downFace);
}

CCubeMesh::~CCubeMesh()
{
}
