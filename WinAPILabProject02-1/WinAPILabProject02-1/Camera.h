#pragma once
#include "Mesh.h"

class CViewport {
public:
	int m_nLeft;
	int m_nTop;
	int m_nWidth;
	int m_nHeight;

	CViewport(int nLeft, int nTop, int nWidth, int nHeight) { m_nLeft = nLeft; m_nTop = nTop; m_nWidth = nWidth; m_nHeight = nHeight; }
	virtual ~CViewport() { }
};

class CCamera
{
private:
	//	카메라의 위치(월드 좌표계)
	float m_fposX = 0.f;
	float m_fposY = 0.f;
	float m_fposZ = 0.f;

	//	카메라의 회전(양의 방향)
	float m_fpitch = 0.f;	//	x축 방향 회전
	float m_fyaw = 0.f;		//	y축 방향 회전
	float m_froll = 0.f;	//	z축 방향 회전

	float m_fFOVAngle = 90.f;					//	시야각
	float m_fProjectRectDistance = 1.f;			//	투영 사각형까지의 거리

	CViewport* m_pViewport = NULL;				//	뷰포트
	float m_AspectRatio = float(FRAMEBUFF_WIDTH) / float(FRAMEBUFF_HEIGHT);	//	종횡비

public:
	CPoint3D CameraTransform(CPoint3D& f3World);
	CPoint3D ProjectionTransform(CPoint3D& f3Camera);
	CPoint3D ScreenTransform(CPoint3D& f3Projection);

	void SetPosition(float x, float y, float z) { m_fposX = x; m_fposY = y; m_fposZ = z; }
	void SetRotation(float pitch, float yaw, float roll) { m_fpitch = pitch; m_fyaw = yaw; m_froll = roll; }

	//	카메라의 뷰포트 설정
	void SetViewport(int xStart, int yStart, int nWidth, int nHeight);
	//	카메라의 시야각 설정
	void SetFOVAngle(float FOVAngle);

	//	각 축으로의 이동 함수
	void Move(float x, float y, float z) { m_fposX += x; m_fposY += y; m_fposZ += z; }

	//	각 축으로의 회전 함수
	void Rotate(float x, float y, float z) { m_fpitch += x; m_fyaw += y; m_froll += z; }
};

