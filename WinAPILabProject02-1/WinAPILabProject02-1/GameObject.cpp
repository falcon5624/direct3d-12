#include "GameObject.h"
#include "MyMath.h"


CGameObject::~CGameObject()
{
	//	게임 객체 인스턴스가 메쉬를 참조하지 않기 때문에 참조값을 1 감소
	if (m_pMesh) m_pMesh->Release();
}

CPoint3D CGameObject::WorldTransform(CPoint3D& f3Model)
{
	float fPitch = DegreeToRadian(m_fpitch);
	float fYaw = DegreeToRadian(m_fyaw);
	float fRoll = DegreeToRadian(m_froll);

	CPoint3D f3World = f3Model;
	CPoint3D f3Rotated = f3Model;

	//	회전 변환
	if (!isZero(fPitch)) {
		f3Rotated.y = f3World.y * cosf(fPitch) - f3World.z * sinf(fPitch);
		f3Rotated.z = f3World.y * sinf(fPitch) + f3World.z * cosf(fPitch);
		f3World.y = f3Rotated.y;
		f3World.z = f3Rotated.z;
	}
	if (!isZero(fYaw)) {
		f3Rotated.x = f3World.x * cosf(fYaw) + f3World.z * sinf(fYaw);
		f3Rotated.z = -f3World.x * sinf(fYaw) + f3World.z * cosf(fYaw);
		f3World.x = f3Rotated.x;
		f3World.z = f3Rotated.z;
	}
	if (!isZero(fRoll)) {
		f3Rotated.x = f3World.x * cosf(fRoll) - f3World.y * sinf(fRoll);
		f3Rotated.y = f3World.x * sinf(fRoll) + f3World.y * cosf(fRoll);
		f3World.x = f3Rotated.x;
		f3World.y = f3Rotated.y;
	}

	//	평행 이동 변환
	f3World.x += m_fposX;
	f3World.y += m_fposY;
	f3World.z += m_fposZ;

	return (f3World);
}


void CGameObject::Animate(float fElapsedTime)
{
	Rotate(m_fpitchSpeed * fElapsedTime, m_fyawSpeed * fElapsedTime, m_frollSpeed * fElapsedTime);
	AnimateColor(fElapsedTime);
}

void CGameObject::AnimateColor(float fElapsedTime)
{
	float red = m_3fcolor.x + m_3fcolorSpeed.x * fElapsedTime;
	float green = m_3fcolor.y + m_3fcolorSpeed.y * fElapsedTime;
	float blue = m_3fcolor.z + m_3fcolorSpeed.z * fElapsedTime;

	if (int(red) > 255) {
		red = 0.f;
	}
	if (int(green) > 255) {
		green = 0.f;
	}
	if (int(blue) > 255) {
		blue = 0.f;
	}

	m_3fcolor = CPoint3D(red, green, blue);
	m_dwColor = RGB(int(red), int(green), int(blue));
}

void CGameObject::Render(HDC hDCFrameBuffer)
{
	HPEN hPen = ::CreatePen(PS_SOLID, 0, m_dwColor);
	HPEN hOldPen = (HPEN)::SelectObject(hDCFrameBuffer, hPen);

	if (m_pMesh) m_pMesh->Render(hDCFrameBuffer);

	::SelectObject(hDCFrameBuffer, hOldPen);
	::DeleteObject(hPen);
}
