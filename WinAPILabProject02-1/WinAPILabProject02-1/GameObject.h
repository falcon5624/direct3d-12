#pragma once
#include "Mesh.h"

class CGameObject
{
private:
	//	게임 객체의 위치(월드변환)
	float m_fposX = 0.f;
	float m_fposY = 0.f;
	float m_fposZ = 0.f;

	//	게임 객체의 회전량(양의 방향)
	float m_fpitch = 0.f;	//	x축 방향 회전
	float m_fyaw = 0.f;		//	y축 방향 회전
	float m_froll = 0.f;	//	z축 방향 회전

	//	게임 객체의 회전속도
	float m_fpitchSpeed = 0.f;
	float m_fyawSpeed = 0.f;
	float m_frollSpeed = 0.f;

	//	게임 객체의 메쉬
	CMesh* m_pMesh = NULL;
	//	게임 객체의 색상(선분의 색상)
	DWORD m_dwColor = RGB(0, 0, 0);
	//	게임 객체의 색상 Float 형태(애니메이션을 위함)
	CPoint3D m_3fcolor;
	//	색상의 변화 속도
	CPoint3D m_3fcolorSpeed;

public:
	CGameObject() { }
	~CGameObject();

	void SetMesh(CMesh* pMesh) { m_pMesh = pMesh; if (pMesh) pMesh->AddRef(); }
	void SetColor(DWORD dwColor) { m_dwColor = dwColor; m_3fcolor.x = float(GetRValue(m_dwColor)); m_3fcolor.y = float(GetGValue(m_dwColor)); m_3fcolor.z = float(GetBValue(m_dwColor)); }
	void SetPosition(float x, float y, float z) { m_fposX = x; m_fposY = y; m_fposZ = z; }
	void SetRotation(float pitch, float yaw, float roll) { m_fpitch = pitch; m_fyaw = yaw; m_froll = roll; }
	void SetColorSpeed(float rSpeed, float gSpeed, float bSpeed) { m_3fcolorSpeed.x = rSpeed; m_3fcolorSpeed.y = gSpeed; m_3fcolorSpeed.z = bSpeed; }
	void SetRotationSpeed(float pitchSpeed, float yawSpeed, float rollSpeed) { m_fpitchSpeed = pitchSpeed; m_fyawSpeed = yawSpeed; m_frollSpeed = rollSpeed; }

	//	각 축으로의 이동 함수
	void Move(float x, float y, float z) { m_fposX += x; m_fposY += y; m_fposZ += z; }

	//	각 축으로의 회전 함수
	void Rotate(float x, float y, float z) { m_fpitch += x; m_fyaw += y; m_froll += z; }

	//	메쉬의 정점 하나를 게임 객체의 위치와 방향을 사용해 월드 변환한다.
	CPoint3D WorldTransform(CPoint3D& f3Model);

	//	애니메이션 함수
	virtual void Animate(float fElapsedTime);
	void AnimateColor(float fElapsedTime);

	//	렌더링 함수
	virtual void Render(HDC hDCFrameBuffer);
};

