﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <windows.h>
// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <cmath>

constexpr int FRAMEBUFF_WIDTH = 800;
constexpr int FRAMEBUFF_HEIGHT = 600;

constexpr int CLIENT_WIDTH = FRAMEBUFF_WIDTH;
constexpr int CLIENT_HEIGHT = FRAMEBUFF_HEIGHT;

//	모니터 크기를 계산합니다.
const int MAX_MONITOR_WIDTH{ GetSystemMetrics(SM_CXSCREEN) };
const int MAX_MONITOR_HEIGHT{ GetSystemMetrics(SM_CYSCREEN) };