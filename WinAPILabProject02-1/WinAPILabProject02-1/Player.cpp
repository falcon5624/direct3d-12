#include "Player.h"

void CPlayer::SetPosition(float x, float y, float z)
{
	/* 플레이어와 카메라의 위치 설정 */
	CGameObject::SetPosition(x, y, z);
	if (m_pCamera) m_pCamera->SetPosition(x, y, z);
}

void CPlayer::SetRotation(float pitch, float yaw, float roll)
{
	/* 플레이어와 카메라의 회전각도 설정 */
	CGameObject::SetRotation(pitch, yaw, roll);
	if (m_pCamera) m_pCamera->SetRotation(pitch, yaw, roll);
}

void CPlayer::Move(float x, float y, float z)
{
	/* 플레이어와 카메라 이동 */
	if (m_pCamera) m_pCamera->Move(x, y, z);
	CGameObject::Move(x, y, z);
}

void CPlayer::Rotate(float x, float y, float z)
{
	/* 플레이어와 카메라 회전 */
	if (m_pCamera) m_pCamera->Rotate(x, y, z);
	CGameObject::Rotate(x, y, z);
}
