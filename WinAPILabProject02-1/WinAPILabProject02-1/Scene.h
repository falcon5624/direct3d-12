#pragma once
#include "GameObject.h"
#include "Player.h"

class CScene
{
private:
	UINT m_nObjects = 0;				//	���� ��ü���� ����
	CGameObject** m_ppObjects = NULL;	//	��ü���� �迭

public:
	CScene() { }
	virtual ~CScene() { }

	//	��ü�� ����
	virtual void BuildObjects();
	//	��ü�� �Ҹ�
	virtual void ReleaseObjects();
	//	��ü�� �ִϸ��̼�
	virtual void Animate(float fElapsedTime);
	//	��ü�� ������
	virtual void Render(HDC hDCFrameBuffer, CCamera* pCamera);
};

