#include "Mesh.h"
#include "GraphicsPipeline.h"

void Draw2DLine(HDC hDCFrameBuffer, XMFLOAT3& f3PreviousProject, XMFLOAT3& f3CurrentProject)
{
	XMFLOAT3 f3Previous =
		CGraphicsPipeline::ScreenTransform(f3PreviousProject);
	XMFLOAT3 f3Current =
		CGraphicsPipeline::ScreenTransform(f3CurrentProject);
	::MoveToEx(hDCFrameBuffer, (long)f3Previous.x, (long)f3Previous.y,
		NULL);
	::LineTo(hDCFrameBuffer, (long)f3Current.x, (long)f3Current.y);
}

CPolygon::CPolygon(UINT nVertices)
{
	m_nVertices = nVertices;
	m_pVertices = new CVertex[nVertices];
}

CPolygon::~CPolygon()
{
	if (m_pVertices) delete[] m_pVertices;
}

void CPolygon::SetVertex(int nIndex, CVertex vertex)
{
	if ((0 <= nIndex) && (nIndex < m_nVertices) && m_pVertices) {
		m_pVertices[nIndex] = vertex;
	}
}

CMesh::CMesh(UINT nPolygons)
{
	m_nPolygons = nPolygons;
	m_ppPolygons = new CPolygon * [nPolygons];
}

CMesh::~CMesh()
{
	if (m_ppPolygons) {
		for (UINT i = 0; i < m_nPolygons; ++i) {
			if (m_ppPolygons) delete m_ppPolygons[i];
		}
		delete[] m_ppPolygons;
	}
}

void CMesh::SetPolygon(int nIndex, CPolygon* pPolygon)
{
	//	메쉬의 다각형 설정
	if ((0 <= nIndex) && (nIndex < m_nPolygons)) m_ppPolygons[nIndex] = pPolygon;
}

void CMesh::SetBoundingBox(XMFLOAT3& xmf3Center, XMFLOAT3& xmf3Extents, XMFLOAT4& xmf4Orientation)
{
	m_bobBoundingBox = BoundingOrientedBox(xmf3Center, xmf3Extents, xmf4Orientation);
}

void CMesh::Render(HDC hDCFrameBuffer)
{
	XMFLOAT3 f3InitialProject, f3PreviousProject;
	bool bPreviousInside = false, bInitialInside = false, bCurrentInside
		= false, bIntersectInside = false;
	for (int j = 0; j < m_nPolygons; j++)
	{
		int nVertices = m_ppPolygons[j]->m_nVertices;
		CVertex* pVertices = m_ppPolygons[j]->m_pVertices;
		f3PreviousProject = f3InitialProject =
			CGraphicsPipeline::Project(pVertices[0].m_xmf3Position);
		bPreviousInside = bInitialInside = (-1.0f <= f3InitialProject.x)
			&& (f3InitialProject.x <= 1.0f) && (-1.0f <= f3InitialProject.y) &&
			(f3InitialProject.y <= 1.0f);
		for (int i = 1; i < nVertices; i++)
		{
			XMFLOAT3 f3CurrentProject =
				CGraphicsPipeline::Project(pVertices[i].m_xmf3Position);
			bCurrentInside = (-1.0f <= f3CurrentProject.x) &&
				(f3CurrentProject.x <= 1.0f) && (-1.0f <= f3CurrentProject.y) &&
				(f3CurrentProject.y <= 1.0f);
			if (((0.0f <= f3CurrentProject.z) && (f3CurrentProject.z <=
				1.0f)) && ((bCurrentInside || bPreviousInside)))
				::Draw2DLine(hDCFrameBuffer, f3PreviousProject, f3CurrentProject);
			f3PreviousProject = f3CurrentProject;
			bPreviousInside = bCurrentInside;
		}
		if (((0.0f <= f3InitialProject.z) && (f3InitialProject.z <= 1.0f))
			&& ((bInitialInside || bPreviousInside))) ::Draw2DLine(hDCFrameBuffer,
				f3PreviousProject, f3InitialProject);
	}
}

CCubeMesh::CCubeMesh(float fWidth, float fHeight, float fDepth) : CMesh(6)
{
	/* 0, 0, 0 모델 좌표계 원점 기준
		width	x축 변
		height	y축 변
		depth	z축 변

		winding order - CW (LHS)
	*/

	CPolygon* forwardFace = new CPolygon(4);	//	전
	CPolygon* backwardFace = new CPolygon(4);	//	후
	CPolygon* leftFace = new CPolygon(4);		//	좌
	CPolygon* rightFace = new CPolygon(4);		//	우
	CPolygon* upFace = new CPolygon(4);			//	상
	CPolygon* downFace = new CPolygon(4);		//	하

	forwardFace->SetVertex(0, CVertex(-fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	forwardFace->SetVertex(1, CVertex(fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	forwardFace->SetVertex(2, CVertex(fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	forwardFace->SetVertex(3, CVertex(-fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));

	leftFace->SetVertex(0, CVertex(-fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	leftFace->SetVertex(1, CVertex(-fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	leftFace->SetVertex(2, CVertex(-fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	leftFace->SetVertex(3, CVertex(-fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));

	rightFace->SetVertex(0, CVertex(fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	rightFace->SetVertex(1, CVertex(fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	rightFace->SetVertex(2, CVertex(fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	rightFace->SetVertex(3, CVertex(fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));

	backwardFace->SetVertex(0, CVertex(fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	backwardFace->SetVertex(1, CVertex(-fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	backwardFace->SetVertex(2, CVertex(-fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	backwardFace->SetVertex(3, CVertex(fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));

	upFace->SetVertex(0, CVertex(-fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));
	upFace->SetVertex(1, CVertex(-fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	upFace->SetVertex(2, CVertex(fWidth / 2.f, fHeight / 2.f, -fDepth / 2.f));
	upFace->SetVertex(3, CVertex(fWidth / 2.f, fHeight / 2.f, fDepth / 2.f));

	downFace->SetVertex(0, CVertex(fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));
	downFace->SetVertex(1, CVertex(fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	downFace->SetVertex(2, CVertex(-fWidth / 2.f, -fHeight / 2.f, fDepth / 2.f));
	downFace->SetVertex(3, CVertex(-fWidth / 2.f, -fHeight / 2.f, -fDepth / 2.f));

	
	SetPolygon(0, forwardFace);
	SetPolygon(1, backwardFace);
	SetPolygon(2, leftFace);
	SetPolygon(3, rightFace);
	SetPolygon(4, upFace);
	SetPolygon(5, downFace);
}

CCubeMesh::~CCubeMesh()
{
}


CAirplaneMesh::CAirplaneMesh(float fWidth, float fHeight, float fDepth) : CMesh(24)
{
	float fx = fWidth * 0.5f, fy = fHeight * 0.5f, fz = fDepth * 0.5f;
	float x1 = fx * 0.2f, y1 = fy * 0.2f, x2 = fx * 0.1f, y3 = fy * 0.3f,
		y2 = ((y1 - (fy - y3)) / x1) * x2 + (fy - y3);
	int i = 0;
	//비행기 메쉬의 위쪽 면
	CPolygon* pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), -fz));
	pFace->SetVertex(1, CVertex(+x1, -y1, -fz));
	pFace->SetVertex(2, CVertex(0.0f, 0.0f, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), -fz));
	pFace->SetVertex(1, CVertex(0.0f, 0.0f, -fz));
	pFace->SetVertex(2, CVertex(-x1, -y1, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+x2, +y2, -fz));
	pFace->SetVertex(1, CVertex(+fx, -y3, -fz));
	pFace->SetVertex(2, CVertex(+x1, -y1, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(-x2, +y2, -fz));
	pFace->SetVertex(1, CVertex(-x1, -y1, -fz));
	pFace->SetVertex(2, CVertex(-fx, -y3, -fz));
	SetPolygon(i++, pFace);
	//비행기 메쉬의 아래쪽 면
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), +fz));
	pFace->SetVertex(1, CVertex(0.0f, 0.0f, +fz));
	pFace->SetVertex(2, CVertex(+x1, -y1, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), +fz));
	pFace->SetVertex(1, CVertex(-x1, -y1, +fz));
	pFace->SetVertex(2, CVertex(0.0f, 0.0f, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+x2, +y2, +fz));
	pFace->SetVertex(1, CVertex(+x1, -y1, +fz));
	pFace->SetVertex(2, CVertex(+fx, -y3, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(-x2, +y2, +fz));
	pFace->SetVertex(1, CVertex(-fx, -y3, +fz));
	pFace->SetVertex(2, CVertex(-x1, -y1, +fz));
	SetPolygon(i++, pFace);
	//비행기 메쉬의 오른쪽 면
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), -fz));
	pFace->SetVertex(1, CVertex(0.0f, +(fy + y3), +fz));
	pFace->SetVertex(2, CVertex(+x2, +y2, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+x2, +y2, -fz));
	pFace->SetVertex(1, CVertex(0.0f, +(fy + y3), +fz));
	pFace->SetVertex(2, CVertex(+x2, +y2, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+x2, +y2, -fz));
	pFace->SetVertex(1, CVertex(+x2, +y2, +fz));
	pFace->SetVertex(2, CVertex(+fx, -y3, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+fx, -y3, -fz));
	pFace->SetVertex(1, CVertex(+x2, +y2, +fz));
	pFace->SetVertex(2, CVertex(+fx, -y3, +fz));
	SetPolygon(i++, pFace);
	//비행기 메쉬의 뒤쪽/오른쪽 면
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+x1, -y1, -fz));
	pFace->SetVertex(1, CVertex(+fx, -y3, -fz));
	pFace->SetVertex(2, CVertex(+fx, -y3, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(+x1, -y1, -fz));
	pFace->SetVertex(1, CVertex(+fx, -y3, +fz));
	pFace->SetVertex(2, CVertex(+x1, -y1, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, 0.0f, -fz));
	pFace->SetVertex(1, CVertex(+x1, -y1, -fz));
	pFace->SetVertex(2, CVertex(+x1, -y1, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, 0.0f, -fz));
	pFace->SetVertex(1, CVertex(+x1, -y1, +fz));
	pFace->SetVertex(2, CVertex(0.0f, 0.0f, +fz));
	SetPolygon(i++, pFace);
	//비행기 메쉬의 왼쪽 면
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), +fz));
	pFace->SetVertex(1, CVertex(0.0f, +(fy + y3), -fz));
	pFace->SetVertex(2, CVertex(-x2, +y2, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, +(fy + y3), +fz));
	pFace->SetVertex(1, CVertex(-x2, +y2, -fz));
	pFace->SetVertex(2, CVertex(-x2, +y2, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(-x2, +y2, +fz));
	pFace->SetVertex(1, CVertex(-x2, +y2, -fz));
	pFace->SetVertex(2, CVertex(-fx, -y3, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(-x2, +y2, +fz));
	pFace->SetVertex(1, CVertex(-fx, -y3, -fz));
	pFace->SetVertex(2, CVertex(-fx, -y3, +fz));
	SetPolygon(i++, pFace);
	//비행기 메쉬의 뒤쪽/왼쪽 면
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, 0.0f, -fz));
	pFace->SetVertex(1, CVertex(0.0f, 0.0f, +fz));
	pFace->SetVertex(2, CVertex(-x1, -y1, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(0.0f, 0.0f, -fz));
	pFace->SetVertex(1, CVertex(-x1, -y1, +fz));
	pFace->SetVertex(2, CVertex(-x1, -y1, -fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(-x1, -y1, -fz));
	pFace->SetVertex(1, CVertex(-x1, -y1, +fz));
	pFace->SetVertex(2, CVertex(-fx, -y3, +fz));
	SetPolygon(i++, pFace);
	pFace = new CPolygon(3);
	pFace->SetVertex(0, CVertex(-x1, -y1, -fz));
	pFace->SetVertex(1, CVertex(-fx, -y3, +fz));
	pFace->SetVertex(2, CVertex(-fx, -y3, -fz));
	SetPolygon(i++, pFace);
}
