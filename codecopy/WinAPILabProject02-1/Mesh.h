#pragma once
#include "stdafx.h"

class CVertex
{
public:
	CVertex() { }
	CVertex(float x, float y, float z) {
		m_xmf3Position = XMFLOAT3(x, y,
			z);
	}
	virtual ~CVertex() { }
	XMFLOAT3 m_xmf3Position;
};

class CPolygon {
public:
	UINT m_nVertices = 0;
	CVertex* m_pVertices = NULL;

	CPolygon() { }
	CPolygon(UINT nVertices);
	virtual ~CPolygon();

	void SetVertex(int nIndex, CVertex vertex);
};

class CMesh
{
private:
	UINT m_nPolygons = 0;
	CPolygon** m_ppPolygons = NULL;

	UINT m_nReferences = 1;	//	Reference Count, 메쉬가 공유하는 게임객체 수
	/*
		인스턴싱을 위해 메쉬는 게임 객체간 공유될 수 있음.
	*/

	BoundingOrientedBox m_bobBoundingBox;	//	바운딩 박스

public:
	CMesh() {}
	CMesh(UINT nPolygons);
	virtual ~CMesh();

	//	공유될 때마다 Count를 증가시킨다.
	void AddRef() { ++m_nReferences; }

	//	공유되는 객체의 소멸마다 감소시키며, 참조자가 없다면 메쉬를 소멸시킨다.
	void Release() { --m_nReferences; if (!m_nReferences) delete this; }

	void SetPolygon(int nIndex, CPolygon* pPolygon);
	void SetBoundingBox(XMFLOAT3& xmf3Center, XMFLOAT3& xmf3Extents, XMFLOAT4& xmf4Orientation);

	virtual void Render(HDC hDCFrameBuffer);
};

class CCubeMesh : public CMesh {
public:
	CCubeMesh(float fWidth = 4.f, float fHeight = 4.f, float fDepth = 4.f);
	virtual ~CCubeMesh();
};

class CAirplaneMesh : public CMesh
{
public:
	CAirplaneMesh(float fWidth, float fHeight, float fDepth);
	virtual ~CAirplaneMesh() { }
};

