﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <windows.h>
// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <cmath>

#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

//	Direct3D 헤더 파일
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>

//	편의성을 위한 이름 공간 선언
using namespace DirectX;
using namespace DirectX::PackedVector;

constexpr int FRAMEBUFF_WIDTH = 800;
constexpr int FRAMEBUFF_HEIGHT = 600;

constexpr int CLIENT_WIDTH = FRAMEBUFF_WIDTH;
constexpr int CLIENT_HEIGHT = FRAMEBUFF_HEIGHT;

//	모니터 크기를 계산합니다.
const int MAX_MONITOR_WIDTH{ GetSystemMetrics(SM_CXSCREEN) };
const int MAX_MONITOR_HEIGHT{ GetSystemMetrics(SM_CYSCREEN) };

#define DIR_FORWARD		0x01
#define DIR_BACKWARD	0x02
#define DIR_LEFT		0x04
#define DIR_RIGHT		0x08
#define DIR_UP			0x10
#define DIR_DOWN		0x20

namespace Matrix4x4
{
	inline XMFLOAT4X4 Identity()
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixIdentity());
		return(xmmtx4x4Result);
	}
}