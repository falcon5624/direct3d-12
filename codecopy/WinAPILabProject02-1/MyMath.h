#pragma once
#include <cfloat>

bool isZero(float number);
bool isEqual(float lhs, float rhs);

/* byte shift */
//#define GetRValue(rgb)      ((BYTE)(rgb))
//#define GetGValue(rgb)      ((BYTE)(((WORD)(rgb)) >> 8))
//#define GetBValue(rgb)      ((BYTE)((rgb)>>16))

namespace Vector3 {
	/* XMFloat3을 로딩하여 XMVector로 SIMD 연산을 수행한 후, 다시 적재하기 위한 이름공간 */

	XMFLOAT3 Add(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	XMFLOAT3 Subtract(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	XMFLOAT3 ScalarProduct(const XMFLOAT3& xmf3, float fScalar);
	float DotProduct(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	XMFLOAT3 CrossProduct(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	XMFLOAT3 Normalize(const XMFLOAT3& xmf3);
	XMFLOAT3 TransformNormal(const XMFLOAT3 xmf3, const XMFLOAT4X4 xmf4x4);
	XMFLOAT3 TransformCoord(const XMFLOAT3 xmf3, const XMFLOAT4X4 xmf4x4);
	float GetLength(const XMFLOAT3& xmf3);
	float GetDistance(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	float GetAngle(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	bool isEqual(const XMFLOAT3& xmf3_lhs, const XMFLOAT3& xmf3_rhs);
	bool isZeroVector(const XMFLOAT3& xmf3);
}

namespace Matrix4x4 {
	/* XMFloat4X4를 로딩하여 XMMatrix로 SIMD 연산을 수행한 후, 다시 적재하기 위한 이름공간 */

	XMFLOAT4X4 MakeIdentity();
	XMFLOAT4X4 Multiply(const XMFLOAT4X4& xmf4x4_lhs, const XMFLOAT4X4& xmf4x4_rhs);
	XMFLOAT4X4 Inverse(const XMFLOAT4X4& xmf4x4);
	XMFLOAT4X4 Transpose(const XMFLOAT4X4& xmf4x4);
	XMFLOAT4X4 RotationYawPitchRoll(float fYaw, float fPitch, float fRoll);
	XMFLOAT4X4 RotationAxis(const XMFLOAT3& xmfAxis, float fAngle);
	XMFLOAT4X4 PerspectiveForLH(float fFovy, float fAspect, float fNear, float fFar);
	XMFLOAT4X4 LookAtLH(const XMFLOAT3& xmf3Eye, const XMFLOAT3& xmf3Focus, const XMFLOAT3& xmf3Up);
}
